package agenda.model.base;

import agenda.exceptions.InvalidFormatException;

public class Contact {
	private String name;
	private String address;
	private String telefon;
	
	public Contact(){
		name = "";
		address = "";
		telefon = "";
	}
	
	public Contact(String name, String address, String telefon) throws InvalidFormatException{

		this.name = name;
		this.address = address;
		this.telefon = telefon;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) throws InvalidFormatException {
		if (!validName(name)) throw new InvalidFormatException("Cannot convert", "Invalid name");
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) throws InvalidFormatException {
		if (!validAddress(address)) throw new InvalidFormatException("Cannot convert", "Invalid address");
		this.address = address;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) throws InvalidFormatException {
		if (!validTelefon(telefon)) throw new InvalidFormatException("Cannot convert", "Invalid phone number");
		this.telefon = telefon;
	}

	public static Contact fromString(String str, String delim) throws InvalidFormatException
	{
		String[] s = str.split(delim);
		if (s.length!=4) throw new InvalidFormatException("Cannot convert", "Invalid data");
		if (!validTelefon(s[2])) throw new InvalidFormatException("Cannot convert", "Invalid phone number");
		if (!validName(s[0])) throw new InvalidFormatException("Cannot convert", "Invalid name");
		if (!validAddress(s[1])) throw new InvalidFormatException("Cannot convert", "Invalid address");
		
		return new Contact(s[0], s[1], s[2]);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append("#");
		sb.append(address);
		sb.append("#");
		sb.append(telefon);
		sb.append("#");
		return sb.toString();
	}
	
	private static boolean validName(String str)
	{
		
		String[] s = str.split("[\\p{Punct}\\s]+");
		if (s.length>2) return false;
		return true;
	}
	
	private static boolean validAddress(String str)
	{
		return true;
	}
	
	private static boolean validTelefon(String tel)
	{
		String[] s = tel.split("[\\p{Punct}\\s]+");
		if (tel.charAt(0) == '+' && s.length == 2 ) return true;
		if (tel.charAt(0) != '0')return false;
		if (s.length != 1) return false;
		return true;
	}
	
		
	@Override
	public boolean equals(Object obj) {
		if (! (obj instanceof Contact)) return false;
		Contact o = (Contact)obj;
		if (name.equals(o.name) && address.equals(o.address) &&
				telefon.equals(o.telefon))
			return true;
		return false;
	}
	
}
