package agenda.startApp;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Contact;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainClassTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testAddContact() throws InvalidFormatException {
        Contact c = new Contact("calin","brazilor","07");

        assertEquals("calin",c.getName());
    }
}