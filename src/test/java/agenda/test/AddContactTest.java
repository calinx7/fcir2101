package agenda.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import agenda.exceptions.InvalidFormatException;

import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryContact;


public class AddContactTest {

	private Contact con;
	private RepositoryContact rep;
	
	@Before
	public void setUp() throws Exception {
		rep = new RepositoryContactMock();
	}
	
	@Test
	public void VALIDtestCase1() throws InvalidFormatException {
		try {
			con = new Contact("name", "", "+40711445566");
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
		//int n = rep.count();
		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
		//assertTrue(n+1 == rep.count());
	}
	
	@Test
	public void INVALIDtestCase2()
	{
		try{
			rep.addContact((Contact) new Object());
		}
		catch(Exception e)
		{
			assertTrue(true);
		}	
	}

	@Test
	public void VALIDtestCase3() throws InvalidFormatException {
		try {
			con = new Contact("name", "", "+40711445566");
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
		//int n = rep.count();
		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
		//assertTrue(n+1 == rep.count());
	}
	@Test
	public void INVALIDtestCase4()
	{
		try{
			rep.addContact((Contact) new Object());
		}
		catch(Exception e)
		{
			assertTrue(true);
		}
	}

	@Test
	public void VALIDtestCase5() throws InvalidFormatException {
		try{
			con = con = new Contact("calin", "", "+4071122334455");
			assertTrue(con.getName().getClass()==String.class);

		}
		catch (Exception e){
			assertTrue(true);
		}
	}
	@Test
	public void INVALIDtestCase6() throws InvalidFormatException {
		try{
			con = con = new Contact("calin", "", "072837192");
			assertTrue(con.getTelefon().length()==9);

		}
		catch (Exception e){
			assertTrue(false);
		}
	}
	@Test
	public void INVALIDtestCase7() throws InvalidFormatException {
		try{
			con = con = new Contact("calin", "", "07283719223");
			assertTrue(con.getTelefon().length()==11);

		}
		catch (Exception e){
			assertTrue(false);
		}
	}
	@Test
	public void VALIDtestCase8() throws InvalidFormatException {
		try{
			con = con = new Contact("calin", "", "0728371932");
			assertTrue(con.getTelefon().length()==10);

		}
		catch (Exception e){
			assertTrue(false);
		}
	}
}
