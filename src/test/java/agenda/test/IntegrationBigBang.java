package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class IntegrationBigBang {

    private Activity act;
    private RepositoryActivity rep;
    private Contact con;
    private RepositoryContact repCon;

    @Before
    public void setUp() throws Exception {
        rep = new RepositoryActivityMock();
        repCon = new RepositoryContactMock();
    }

    @Test
    public void testCase1()
    {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            act = new Activity("name1",
                    df.parse("03/20/2013 12:00"),
                    df.parse("03/20/2013 13:00"),
                    null,
                    "Lunch break");
            rep.addActivity(act);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assertTrue(1 == rep.count());
    }

    @Test
    public void VALIDtestCase1() throws InvalidFormatException {
        try {
            con = new Contact("name", "", "+40711445566");
        } catch (InvalidFormatException e) {
            assertTrue(true);
        }
        //int n = rep.count();
        repCon.addContact(con);
        for(Contact c : repCon.getContacts())
            if (c.equals(con))
            {
                assertTrue(true);
                break;
            }
        //assertTrue(n+1 == rep.count());
    }

    @Test
    public void testCase3() {
        for (Activity act : rep.getActivities())
            rep.removeActivity(act);

        Calendar c = Calendar.getInstance();
        c.set(2013, 3 - 1, 20, 12, 00);
        Date start = c.getTime();

        c.set(2013, 3 - 1, 20, 12, 30);
        Date end = c.getTime();

        Activity act = new Activity("name1", start, end,
                new LinkedList<Contact>(), "description2");

        rep.addActivity(act);

        c.set(2013, 3 - 1, 20);

        List<Activity> result = rep.activitiesOnDate("name1", c.getTime());
        assertTrue(result.size() == 1);
    }



}
